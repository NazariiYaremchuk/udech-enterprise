$(document).ready(function() {
	$('#firstHead').fadeOut(2000);
    $('#secondHead').text('ready!').fadeIn(); 
});

$(document).ready(function() {
	$(window).scroll(function() {
		if($(this).scrollTop() != 0) {
		$('.toTop').fadeIn(1500);
		} else {
		$('.toTop').fadeOut(500);
		}
	});
	$('.toTop').click(function() {
		$('body,html').animate({scrollTop:0},1000);
	});
});